# Autonomous Maze Navigation Robot

Welcome to the project repository for the Autonomous Maze Navigation Robot
developed by myself, [Jenelle
Fernando](https://www.linkedin.com/in/jenelle-fernando/), and [Omar
Mateo](https://www.linkedin.com/in/omar-mateo-056402225/). We developed this
project for our Autonomous Robotic Systems course at [Florida Polytechnic
University](https://floridapoly.edu/).

The robot's goal is to complete an obstacle course without becoming stuck or
crashing into an obstacle. My robot completed the maze in 1 minute and 37
seconds, and we achieved an overall project grade of 100%.

## Demonstration Videos

![](./videos/overhead_perspective.webm)

![](./videos/robot_perspective.webm)

## Hardware

All teammates used the same software to complete the maze, but we built our own
hardware robots, incorporating:

-   A basic robot frame.
-   Four servo motors for locomotion.
-   An Arduino Uno to navigate and control the servo motors.
-   A Raspberry Pi and attached camera to record a video from the robot's
    perspective during maze navigation.
-   A large battery bank to power the Arduino Uno and Raspberry Pi.

![](./images/construction.png)

## Approach

The goal of the robot is to navigate autonomously (meaning without human
guidance or direction) from the start of the obstacle course to the end, without
becoming stuck or "crashing" into any walls or obstacles. We utilized three
ultrasonic obstacle-detection sensors on the front, left and right of the robot
to identify where obstacles are located in the robot's path. Our simple
navigation algorithm programmed in C++ is as follows:

-   If there are no detected obstacles closer than 10 centimeters, move forward.
-   If there is an obstacle detected:
    -   Turn to move away from the obstacle if possible.
    -   Otherwise, if all three sensors detect a object closer than 10
        centimeters, then the robot may become stuck by continuing forward;
        thus, backup, and randomly choose to turn left or right to find a new
        path forward.

I incorporated the last part of the algorithm to back up and try a new path,
since our earlier prototypes repeatedly became stuck in corners. We also
developed a simple system of constant alternation between reading each of the
three sensors and making navigation decisions.

Below are some images visualizing the robot's decision-making process.

![](./images/robot-left.png)

![](./images/robot-right.png)

![](./images/robot-back-1.png)

![](./images/robot-back-2.png)
