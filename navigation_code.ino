/*
 Robotics with the BOE Shield – ServosOppositeDirections
 Generate a servo full speed counterclockwise signal with pin 13 and
 full speed clockwise signal with pin 12.
 */

#include <Servo.h>  // Include servo library

Servo servoLeft;   // Declare left servo signal
Servo servoRight;  // Declare right servo signal

// Define constants for ultrasonic sensor pins
const int frontSensorPing = 12;
const int frontSensorEcho = 6;

const int leftSensorPing = 13;
const int leftSensorEcho = 7;

const int ARRAY_SIZE = 15;
const int MAX_SAME_DIRECTION_COUNT = 5;
int sameDirectionCount = 0;

const int rightSensorPing = 11;
const int rightSensorEcho = 5;

int state = 0;  //State is used to change the commands of the robot

// Range on cm for each of the ultrasonics
int distanceLeft;   //Left Ultrasonic
int distanceFront;  //Front Ultrasonic
int distanceRight;  //Right Ultrasonic
// int distanceBack;     //Back Ultrasonic

//Safe distance in cm.
const int safeDistance = 10;
const int safeDistanceSides = 10;
int prevFront;
int prevLeft;
int prevRight;

// The time (in ms) at which the current backup operation started, or -1 if
// no backup operation is ongoing.
long backup_time{ -1 };

void setup() {
  servoLeft.attach(13);   // Attach left signal to pin 13
  servoRight.attach(12);  // Attach right signal to pin 12
  Serial.begin(9600);     // Starting Serial Terminal
  // Timer1.initialize(500000);
  // Timer1.attachInterrupt(updateDistance);
  // Timer1.start();
  // OCR0A = 5;
  // TIMSK0 |= _BV(OCIE0A);
}

// SIGNAL(TIMER0_COMPA_vect) {
//   if (millis() % 100 == 0) {
//     updateDistance();
//   }
// }


void loop() {
  int left{};
  int front{};
  int right{};
  // if (previous_millis + 30 < millis()) {
  // updateDistance();
  // previous_millis = millis();
  // }
  updateDistance();
  left = distanceLeft;
  front = distanceFront;
  right = distanceRight;
  // updateState(left, front, right);
  backup_time = setCommand(left, front, right, backup_time);
}

void move(int left, int right) {
  servoLeft.writeMicroseconds(left);
  servoRight.writeMicroseconds(right);
}

void moveForward() {
  Serial.println("F");
  move(1580, 1420);
}

void moveBackward() {
  Serial.println("B");
  move(1400, 1600);
}

void turnLeft() {
  Serial.println("L");
  move(1490, 1420);
}

void turnRight() {
  Serial.println("R");
  move(1580, 1510);
}

void stop() {
  move(1500, 1500);
}

// Continue (or start) a backup operation which started at initial_backup_time.
// The robot will backup for 600 ms after initial_backup_time, then turn
// either left or right (randomly chosen) for another 600 ms.
// true is returned if the backup operation is still ongoing, else false
// is returned.
bool continue_backup_operation(long initial_backup_time) {
  enum class ChosenTurn {
    NONE,
    LEFT,
    RIGHT
  };
  static auto chosen_turn{ ChosenTurn::NONE };
  if (millis() < initial_backup_time + 600) {
    moveBackward();
    return true;
  } else if (millis() < initial_backup_time + 1200) {
    if (chosen_turn == ChosenTurn::NONE) {
      chosen_turn = (rand() % 2 == 0) ? ChosenTurn::LEFT : ChosenTurn::RIGHT;
    }
    if (chosen_turn == ChosenTurn::LEFT) {
      turnLeft();
    } else {
      turnRight();
    }
    return true;
  } else {
    chosen_turn = ChosenTurn::NONE;
    return false;
  }
}

long setCommand(int left, int front, int right, long initial_backup_time) {
  Serial.print("Left: ");
  Serial.print(left);
  Serial.print(" | Front: ");
  Serial.print(front);
  Serial.print(" | Right: ");
  Serial.println(right);

  // If a backup operation has already started, indicated by
  // initial_backup_time != -1, continue the backup operation.
  if (initial_backup_time != -1) {
    bool result = continue_backup_operation(initial_backup_time);
    if (result) return initial_backup_time;
    else return -1;
  }

  if (front < safeDistance) {
    // If left and right are too close to each other, return a
    // number != -1 to start a backup operation at the current time in ms.
    if (left - 5 <= right && right <= left + 5) {
      return static_cast<long>(millis());
    } else if (left > right) {
      turnLeft();
    } else {
      turnRight();
    }
  } else if (left < safeDistanceSides) {
    // If left and right are too close to each other, return a
    // number != -1 to start a backup operation at the current time in ms.
    if (left - 5 <= right && right <= left + 5)
      return static_cast<long>(millis());
    else turnRight();
  } else if (right < safeDistanceSides) {
    // If left and right are too close to each other, return a
    // number != -1 to start a backup operation at the current time in ms.
    if (left - 5 <= right && right <= left + 5)
      return static_cast<long>(millis());
    else turnLeft();
  } else {
    moveForward();
  }
  // Return -1 to indicate that no backup operation is ongoing.
  return -1;
}

void updateDistance() {
  enum class NextDirection {
    LEFT,
    FORWARD,
    RIGHT,
  };
  static auto next_direction{ NextDirection::LEFT };
  switch (next_direction) {
    case NextDirection::LEFT:
      distanceLeft = getDistance(leftSensorPing, leftSensorEcho, prevLeft, -1);
      next_direction = NextDirection::FORWARD;
      break;
    case NextDirection::FORWARD:
      distanceFront = getDistance(frontSensorPing, frontSensorEcho, prevFront, 0);
      next_direction = NextDirection::RIGHT;
      break;
    case NextDirection::RIGHT:
      distanceRight = getDistance(rightSensorPing, rightSensorEcho, prevRight, 1);
      next_direction = NextDirection::LEFT;
      break;
  }
}

int getDistance(int pingPin, int echoPin, int prevReading, int whichSensor) {
  long duration, cm;
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
  cm = microsecondsToCentimeters(duration);
  delay(100);
  if (cm < 4) {
    return prevReading;
  } else {
    if (whichSensor == -1) prevLeft = cm;
    if (whichSensor == 0) prevFront = cm;
    if (whichSensor == 1) prevRight = cm;
  }
  return cm;
}


long microsecondsToCentimeters(long microseconds) {
  return microseconds / 29 / 2;
}